# README #

n-folds: number of possible paper folds - code in haskell

### What is this repository for? ###

* find number of folds possible for a stripe of paper of given thickness and length in millimeters
* n-folds.hs for calculating alternate side folding
* n-folds-single.hs for calculating single side folding

### How do I get set up? ###

* install haskell and GHCi - google for that.
* download n-folds.hs and/or n-folds-single.hs
* to compile them: # > ghc --make n-folds.hs
* now to run them: # > ./n-folds  (binary exec file, make it exec if already not)

### Who do I talk to? ###

* Repo owner or ~nullx002